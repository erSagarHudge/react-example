
import React, { PureComponent } from 'react';
import { TouchableOpacity, Dimensions, View, Text } from 'react-native';


export default class HomeScreen extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
        }

    }
    componentDidMount() {

    }
    

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text onPress={() => { this.props.navigation.navigate('progress') }} style={{ padding: 10, fontSize: 16, color: 'black' }}>Eg. Custom Progress</Text>
                <Text  onPress={() => { this.props.navigation.navigate('Detail') }} style={{ padding: 10, fontSize: 16, color: 'black' }}>Eg. Animated View Progress</Text>
                <Text  onPress={() => { this.props.navigation.navigate('List') }} style={{ padding: 10, fontSize: 16, color: 'black' }}>Eg. APi Call And Details</Text>

            </View>
        );
    }
}
