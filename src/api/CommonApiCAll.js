
import axios from 'axios';

export const getAxios = async (url) => {
    return axios.get(url).then(response => {
        return response;
    }).catch(error => {
        if (error.response) {
            return error.response.data;
        } else {
            return error;
        }
    })
}

export const postAxios = async (url, dataObject, token) => {

    var headers = token && token != "" ?
        {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        } : {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }

    return axios.post(url,
        dataObject,
        { headers: headers })
        .then((response) => {
            return response;
        })
        .catch((error) => {
            return error;
        });

}