
import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, View, Animated, TextInput, Text,TouchableOpacity } from 'react-native';
 const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let intervalCount = 1000;
let progressCount = 100;

export default class DetailsScreen extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            number: 0,
            listNumbers: ''
        }

    }
    componentDidMount() {
        //we can increase `intervalCount` to 10 sec its 1000 ms (1 sec) 
        this.animationListeners();
    }

    animationListeners() {
        this.interval = setInterval(() => {
            this.setState({ progress: this.state.progress + 1 });
            this.state.progress === progressCount && clearInterval(this.interval);
        }, intervalCount)
    }

    render() {
        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={styles.progressContainer}>
                    <Animated.View style={[styles.innerContainer, { width: this.state.progress + "%" }]} />
                    <Animated.Text style={styles.completedText}>{this.state.progress}%</Animated.Text>
                </View>
                <Text style={{marginTop:30,paddingLeft:10}}>PRIME NUMBER TEST</Text>
                <TextInput
                    style={{ height: 40, margin: 10 ,borderWidth:1}}
                    placeholder="Type number to get all prime numbers"
                    onChangeText={val => this.setState({ number: val, listNumbers: '' })}
                    defaultValue={this.state.number}/>

                <TouchableOpacity onPress={() => { this.printPrime() }}>
                    <Text style={{ alignSelf: 'center', margin: 10, padding: 10, color: 'red', fontWeight: 'bold' }}>Check</Text>
                </TouchableOpacity>
                {this.state.number != 0 && this.state.listNumbers!='' ? <Text style={{ margin: 10 }}>{`list of all prime number for value ${this.state.number} is \n ${this.state.listNumbers}`}</Text>:null}
            </View>

        );
    }

    printPrime() {
        let values;
        for (let i = 1; i <= this.state.number; i++) {
            let flag = 0;

            for (let j = 2; j < i; j++) {
                if (i % j == 0) {
                    flag = 1;
                    break;
                }
            }

            if (i > 1 && flag == 0) {
                // console.log("numbers", i);
                values = values ? values + "," + i : i
            }
        }
        this.setState({ listNumbers: values })

    }
}
const styles = StyleSheet.create({

    progressContainer: {
        width: windowWidth,
        height: 30,
        borderColor: "green",
        borderWidth: 1,
        borderRadius: 5,
        marginTop: '10%',
        justifyContent: "center",
    },

    innerContainer: {
        height: 30,
        borderRadius: 5,
        backgroundColor: "green",
    },

    completedText: {
        fontSize: 23,
        color: "black",
        position: "absolute",
        zIndex: 1,
        alignSelf: "center",
    }
});