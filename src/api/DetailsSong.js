import React, { PureComponent } from 'react';
import { StyleSheet, FlatList, View, Image, TextInput, Text, TouchableOpacity } from 'react-native';
 export default class DetailsSong extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.route.params.data,
            fullDescription: false

        }
    }
    componentDidMount() {
       
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', padding: 10, backgroundColor: 'white' }}>
                <Image resizeMode={'contain'} source={{ uri: this.state.data.artworkUrl100 }} style={{ width: 150, height: 150 }} />
 {console.log(this.state.data)}
                <Text style={{ paddingVertical: 5, color: 'black', fontWeight: 'bold' }}>{this.state.data.artistName}</Text>
                <Text style={{  color: 'black', fontWeight: 'bold' }}>{this.state.data.primaryGenreName}</Text>
                <Text style={{ paddingVertical: 15 }} onPress={() => { this.setState({ fullDescription: !this.state.fullDescription })}}>{!this.state.fullDescription && this.state.data.shortDescription ? this.state.data.description + " Read More..." : this.state.data.description}</Text>
            </View>)
    }

}