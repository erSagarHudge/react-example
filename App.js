import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './src/HomeScreen'
import DetailsScreen from './src/DetailsScreen'
import { createStackNavigator } from '@react-navigation/stack';
import ProgressBar from './src/ProgressBar';
import ListData from './src/api/ListData';
import DetailsSong from './src/api/DetailsSong';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>

      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen}
          options={{ headerShown: false }} />
        <Stack.Screen name="progress" component={ProgressBar}
          options={{ headerShown: false }} />
        <Stack.Screen name="Detail" component={DetailsScreen} />
        <Stack.Screen name="List" component={ListData} />
        <Stack.Screen name="Details" component={DetailsSong} />
      </Stack.Navigator>

    </NavigationContainer>
  );
}