import React, { PureComponent } from 'react';
import { StyleSheet, FlatList, View, Image, TextInput, Text, TouchableOpacity } from 'react-native';
import { getAxios } from './CommonApiCAll';
export default class ListData extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            error: 'Loading...',
        }
    }
    componentDidMount() {
        let url = "https://itunes.apple.com/search?term=Michael+jackson"
        // let url = "https://reqres.in/api/users?page=2"
        getAxios(url).then((response) => {
            if (response.data && response.data.results) {
                this.setState({ data: response.data.results, error: '' })
            } else {
                this.setState({ error: 'No data availabel' })

            }

        }).catch(e => {
            console.log(e.message)
        })
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                {this.state.error === "" ? <FlatList
                    numColumns={1}
                    data={this.state.data}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => index.toString()}
                /> : <Text style={{ fontSize: 18,fontWeight:'bold', textAlign: 'center', color: 'black' }}>{this.state.error}</Text>
                }
            </View>)
    }
    renderItem({ item }) {
        return (
            <TouchableOpacity onPress={() => { this, this.props.navigation.navigate("Details", { data: item }) }} style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <Image resizeMode={'stretch'} source={{ uri: item.artworkUrl100 }} style={{ width: 50, height: 60, backgroundColor: 'white' }} />

                <View style={{ backgroundColor: 'white', borderBottomColor: 'gray', borderWidth: 0.2, flex: 1, padding: 10 }}>
                    <Text style={{ paddingVertical: 5 }}>Artist : {item.artistName}</Text>
                    <Text>{item.primaryGenreName}</Text>      
                </View>
            </TouchableOpacity >)
    }
}